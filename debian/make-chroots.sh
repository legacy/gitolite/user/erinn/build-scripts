#!/bin/sh

# ubuntu 32

linux32 sudo pbuilder create --distribution intrepid --basetgz /var/cache/pbuilder/base-ubuntu-i386-intrepid.tgz  --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu

linux32 sudo pbuilder create --distribution jaunty --basetgz /var/cache/pbuilder/base-ubuntu-i386-jaunty.tgz  --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu

linux32 sudo pbuilder create --distribution karmic --basetgz /var/cache/pbuilder/base-ubuntu-i386-karmic.tgz  --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu

linux32 sudo pbuilder create --distribution lucid --basetgz /var/cache/pbuilder/base-ubuntu-i386-lucid.tgz  --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu

linux32 sudo pbuilder create --distribution maverick --basetgz /var/cache/pbuilder/base-ubuntu-i386-maverick.tgz  --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu

# ubuntu 64

sudo pbuilder create --distribution intrepid --basetgz /var/cache/pbuilder/base-ubuntu-amd64-intrepid.tgz.tgz  --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu

sudo pbuilder create --distribution jaunty --basetgz /var/cache/pbuilder/base-ubuntu-amd64-jaunty.tgz  --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu

sudo pbuilder create --distribution karmic --basetgz /var/cache/pbuilder/base-ubuntu-amd64-karmic.tgz  --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu

sudo pbuilder create --distribution lucid --basetgz /var/cache/pbuilder/base-ubuntu-amd64-lucid.tgz  --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu

sudo pbuilder create --distribution maverick --basetgz /var/cache/pbuilder/base-ubuntu-amd64-maverick.tgz  --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu

# debian 32

linux32 sudo pbuilder create --distribution lenny --basetgz /var/cache/pbuilder/base-debian-i386-lenny.tgz  --debootstrapopts --arch=i386 --mirror http://ftp.us.debian.org/debian/

linux32 sudo pbuilder create --distribution sid --basetgz /var/cache/pbuilder/base-debian-i386-sid.tgz  --debootstrapopts --arch=i386 --mirror http://ftp.us.debian.org/debian/

linux32 sudo pbuilder create --distribution squeeze --basetgz /var/cache/pbuilder/base-debian-i386-squeeze.tgz  --debootstrapopts --arch=i386 --mirror http://ftp.us.debian.org/debian/

# debian 64

sudo pbuilder create --distribution lenny --basetgz /var/cache/pbuilder/base-debian-amd64-lenny.tgz  --debootstrapopts --arch=amd64 --mirror http://ftp.us.debian.org/debian/

sudo pbuilder create --distribution sid --basetgz /var/cache/pbuilder/base-debian-amd64-sid.tgz  --debootstrapopts --arch=amd64 --mirror http://ftp.us.debian.org/debian/

sudo pbuilder create --distribution squeeze --basetgz /var/cache/pbuilder/base-debian-amd64-squeeze.tgz  --debootstrapopts --arch=amd64 --mirror http://ftp.us.debian.org/debian/
