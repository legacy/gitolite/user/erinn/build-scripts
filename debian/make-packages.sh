#!/bin/sh
PKG=$1
VER=$2-1

# ubuntu 32

linux32 sudo pbuilder build --binary-arch --distribution intrepid --basetgz /var/cache/pbuilder/base-ubuntu-i386-intrepid.tgz --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~intrepid1.dsc

linux32 sudo pbuilder build --binary-arch --distribution jaunty --basetgz /var/cache/pbuilder/base-ubuntu-i386-jaunty.tgz --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~jaunty1.dsc

linux32 sudo pbuilder build --binary-arch --distribution karmic --basetgz /var/cache/pbuilder/base-ubuntu-i386-karmic.tgz --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~karmic1.dsc

linux32 sudo pbuilder build --binary-arch --distribution lucid --basetgz /var/cache/pbuilder/base-ubuntu-i386-lucid.tgz --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~lucid1.dsc

linux32 sudo pbuilder build --binary-arch --distribution maverick --basetgz /var/cache/pbuilder/base-ubuntu-i386-maverick.tgz --debootstrapopts --arch=i386 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~maverick1.dsc

# ubuntu 64

sudo pbuilder build --distribution intrepid --basetgz /var/cache/pbuilder/base-ubuntu-amd64-intrepid.tgz --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~intrepid1.dsc

sudo pbuilder build --distribution jaunty --basetgz /var/cache/pbuilder/base-ubuntu-amd64-jaunty.tgz --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~jaunty1.dsc

sudo pbuilder build --distribution karmic --basetgz /var/cache/pbuilder/base-ubuntu-amd64-karmic.tgz --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~karmic1.dsc

sudo pbuilder build --distribution lucid --basetgz /var/cache/pbuilder/base-ubuntu-amd64-lucid.tgz --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~lucid1.dsc

sudo pbuilder build --distribution maverick --basetgz /var/cache/pbuilder/base-ubuntu-amd64-maverick.tgz --debootstrapopts --arch=amd64 --mirror http://archive.ubuntu.com/ubuntu ${PKG}_${VER}~maverick1.dsc

# debian 32

linux32 sudo pbuilder build --binary-arch --distribution lenny --basetgz /var/cache/pbuilder/base-debian-i386-lenny.tgz --debootstrapopts --arch=i386 --mirror http://ftp.us.debian.org/debian/ ${PKG}_${VER}~lenny.dsc

linux32 sudo pbuilder build --binary-arch --distribution squeeze --basetgz /var/cache/pbuilder/base-debian-i386-squeeze.tgz --debootstrapopts --arch=i386 --mirror http://ftp.us.debian.org/debian/ ${PKG}_${VER}~squeeze.dsc

linux32 sudo pbuilder build --binary-arch --distribution sid --basetgz /var/cache/pbuilder/base-debian-i386-sid.tgz --debootstrapopts --arch=i386 --mirror http://ftp.us.debian.org/debian/ ${PKG}_${VER}.dsc

# debian 64

sudo pbuilder build --distribution lenny --basetgz /var/cache/pbuilder/base-debian-amd64-lenny.tgz --debootstrapopts --arch=amd64 --mirror http://ftp.us.debian.org/debian/ ${PKG}_${VER}~lenny.dsc

sudo pbuilder build --distribution squeeze --basetgz /var/cache/pbuilder/base-debian-amd64-squeeze.tgz --debootstrapopts --arch=amd64 --mirror http://ftp.us.debian.org/debian/ ${PKG}_${VER}~squeeze.dsc

sudo pbuilder build --distribution sid --basetgz /var/cache/pbuilder/base-debian-amd64-sid.tgz --debootstrapopts --arch=amd64 --mirror http://ftp.us.debian.org/debian/ ${PKG}_${VER}.dsc


echo "Results of package building:"
ls -al /var/cache/pbuilder/result
